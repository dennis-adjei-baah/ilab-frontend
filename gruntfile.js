
module.exports = function (grunt) {
    var NODE_DIR = 'node_modules';
    grunt.initConfig({
        concat: {
            internal: {
                src: [
                    NODE_DIR + '/jquery/dist/jquery.js',
                    NODE_DIR + '/bootstrap/dist/js/bootstrap.js',
                    NODE_DIR + '/lodash/lodash.js',
                    NODE_DIR + '/angular/angular.js',
                    NODE_DIR + '/angular-route/angular-route.js',
                    NODE_DIR + '/angular-animate/angular-animate.js',
                    NODE_DIR + '/angular-sanitize/angular-sanitize.js',
                    NODE_DIR + '/ui-select/dist/select.js',
                    NODE_DIR + '/angularjs-toaster/toaster.js',
                    NODE_DIR + '/restangular/dist/restangular.js',
                    NODE_DIR + '/d3/d3.js',
                    NODE_DIR + '/nvd3/build/nv.d3.js',
                    NODE_DIR + '/angular-nvd3/dist/angular-nvd3.js'
                ],
                dest: 'dist/internal.js'
            },
            dist: {
                src: ['app/*.js', 'app/**/*.js'],
                dest: 'dist/build.js'
            }
        },
        watch: {
            scripts: {
                files: ['app/**/*.js'],
                tasks: ['build'],
                livereload: true
            },
            less: {
                files: ['app/**/*.less', 'app/*.less'],
                tasks: ['build'],
                livereload: true
            }
        },
        less: {
            development: {
                files: {
                    'dist/styles.css': 'app/all.less'
                }
            }
        },
        copy: {

            main: {
                expand: true,
                cwd: 'node_modules/font-awesome/fonts',
                src: '**',
                dest: 'fonts/'
            },
            nvd3:{
                expand:true,
                cwd:'node_modules/nvd3/build',
                src:'nv.d3.css',
                dest:'dist/'
            },
            uiselect:{
                expand:true,
                cwd:'node_modules/ui-select/dist',
                src:'select.css',
                dest:'dist/'
            },
            toaster:{
                expand:true,
                cwd:'node_modules/angularjs-toaster',
                src:'toaster.css',
                dest:'dist/'
            }

        },
        express: {
            all: {
                options: {
                    port: 3000,
                    hostname: 'localhost',
                    bases: ['app/**/', 'index.html'],
                    livereload: true
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-express');


    grunt.registerTask('build', ['concat', 'less', 'copy']);
    grunt.registerTask('myWatch', ['concat', 'watch']);
    grunt.registerTask('serve', ['express', 'watch']);
};
(function () {
    var app = angular.module('ilab');
    app.controller('NavCtrl', ['$scope', '$location', function ($scope, $location) {
        $scope.routeMatches = function (path) {
            return $location.path().match(path);
        }
    }]);
})(window.angular);
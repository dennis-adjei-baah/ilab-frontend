(function () {
    var appDashboard = angular.module('ilab.dashboard');
    appDashboard.controller('DashboardCtrl', ['$scope','Reports', function ($scope, Reports) {
        $scope.company = {};
        $scope.data = [{
            key: "Total Gross Revenue",
            values: [],
            area: true,
            type: "bar",
            color: '#8cbc4f'
        }];



        $scope.countMetrics = [
            {
                countMetric: "Clients",
                count: 0
            },
            {
                countMetric: "Graduates",
                count: 102
            },
            {
                countMetric: "Merged",
                count: 2
            },
            {
                countMetric: "Full-time Emp.",
                count: 200
            },
            {
                countMetric: "Part-time Emp.",
                count: 30
            }
        ];

        $scope.options = {
            chart: {
                type: 'lineChart',
                height: 450,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 55
                },
                x: function (d) {
                    return d.x;
                },
                y: function (d) {
                    return d.y;
                },
                showValues: false,
                transitionDuration: 500,
                xAxis: {
                    axisLabel: 'YoY (Year over Year)'
                },
                yAxis: {
                    axisLabel: 'Revenue ($)',
                    axisLabelDistance: 0,
                    tickFormat:function (d) {
                        return d3.format('$,')(d);
                    }
                }
            }
        };

        function populateGraph(data){
            console.log(data);
            var chartData = [];
            angular.forEach(data, function(value, key){
                chartData.push({'x':value['Year'].toString(),'y':Number(value['Gross_Revenue'])});
            });
            $scope.data[0].values = chartData;
        }

        function populateCountMetric(countMetrics) {
            console.log(countMetrics);
            $scope.countMetrics = countMetrics[0].plain();
        }

        function init(){
            Reports
                .revenue
                .getList({transform:1})
                .then(populateGraph);
            Reports
                .ILabMetrics
                .getList({transform:1})
                .then(populateCountMetric);
        }

        init();

    }]);
})(window.angular);
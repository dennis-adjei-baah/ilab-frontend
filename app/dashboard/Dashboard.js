(function () {
    var clients = angular.module('ilab.services');
    clients.service('Reports', ['Restangular', function (Restangular) {

        var reports = function () {
        };
        reports.revenue = Restangular.service('Total_Gross_Revenue_By_Year');
        reports.funds = Restangular.service('Grant_Funds_By_Year');
        reports.ILabMetrics= Restangular.service('ILabMetrics');

        return reports;
    }])
})();


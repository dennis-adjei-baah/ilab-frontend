(function () {
    var app = angular.module('ilab', [
        'ngRoute',
        'ilab.dashboard',
        'ilab.clients',
        'ilab.reports',
        'ilab.events',
        'restangular',
        'toaster',
        'ngAnimate',
        'nvd3',
        'ui.select',
        'ngSanitize'
    ]);
    var services = angular.module('ilab.services', []);
    var dashboard = angular.module('ilab.dashboard', ['ilab.services']);
    var clients = angular.module('ilab.clients', []);
    var reports = angular.module('ilab.reports',['ilab.clients']);
    var events = angular.module('ilab.events', []);


    app.config(function ($routeProvider, RestangularProvider) {
        $routeProvider
            .when('/dashboard', {templateUrl: 'app/dashboard/dashboard.html'})
            .when('/reports', {templateUrl: 'app/reports/reports.html'})
            .when('/companies', {templateUrl: 'app/clients/clients.html'})
            .when('/companies/add', {templateUrl: 'app/clients/add-company.html'})
            .when('/companies/:id', {templateUrl: 'app/clients/client-info.html'})
            .when('/companies/:id/edit', {templateUrl: 'app/clients/add-company.html'})
            .when('/individuals', {templateUrl: 'app/clients/clients.html'})
            .when('/individuals/add', {templateUrl: 'app/clients/add-individual.html'})
            .when('/individuals/:id', {templateUrl: 'app/clients/individual-info.html'})
            .when('/individuals/:id/edit', {templateUrl: 'app/clients/add-individual.html'})
            .when('/events', {templateUrl: 'app/events/events.html'})
            .when('/upload',{templateUrl:'app/survey-upload/upload.html'})
            .otherwise('/dashboard');
        RestangularProvider.setBaseUrl('http://www.cusaac.com/api.php');
        RestangularProvider.setResponseInterceptor(function(data, operation, what){
            var resp;
            if (operation == 'getList') {
                resp =  data[what];
                return resp
            }
            return data;
        });

    });
})(window.angular);
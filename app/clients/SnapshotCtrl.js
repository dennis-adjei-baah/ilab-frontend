(function () {
    var clientsModule = angular.module('ilab.clients');
    clientsModule.controller('SnapshotCtrl', ['$scope', '$routeParams', 'CompanyInfo', 'ReportSurvey', 'Designation', 'Referral', 'Employee',
        function ($scope, $routeParams, CompanyInfo, ReportSurvey, Designation, Referral, Employee) {

        function generateUserAcronym(name) {
            var splitName = name.split(' ');
            var acronym = "";
            for (var i = 0; i < splitName.length; i++) {
                acronym += splitName[i][0];
            }
            return acronym;
        }

            var loadEmployees = function (companyInfo) {
                Employee.getList({
                    transform: 1,
                    filter: 'Company_ID,eq,' + companyInfo.companyId,
                    include: 'Individual'
                })
                    .then(function(employees){
                        $scope.employees = employees;
                    })
            };

        function addCompanyInfo(data) {
            $scope.company = data[0];
            //$scope.company.contactAcronym = generateUserAcronym($scope.company.Contact_Name);
            ReportSurvey
                .getList({transform: 1, filter: 'Company_ID,eq,' + $scope.company.Company_ID})
                .then(function (reports){
                    $scope.reportSurveys = reports;
                });
            Designation
                .getList({transform: 1, filter: 'Company_ID,eq,' + $scope.company.Company_ID})
                .then (function (designations){
                    $scope.designations = designations;
                });
            Referral
                .getList({transform: 1, filter: 'Referree_Entity_ID,eq,' + $scope.company.Entity_ID,include:'Entity,Individual,Company'})
                .then (function (referrals){
                    $scope.referrals = referrals;
                });
            return data;
        }


        function init() {
            CompanyInfo
                .getList({transform: 1, filter: 'Company_ID,eq,' + $routeParams.id})
                .then(addCompanyInfo);
            loadEmployees({companyId:$routeParams.id});
        }

        init();
    }]);
})();

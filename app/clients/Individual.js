(function () {
    var clients = angular.module('ilab.clients');
    clients.service('Individual', ['Restangular', function (Restangular) {
        return Restangular.withConfig(function(RestangularConfigurer){
           RestangularConfigurer.setRestangularFields({
               id:'Individual_ID'
           })
        }).service('Individual');
    }]);
})();
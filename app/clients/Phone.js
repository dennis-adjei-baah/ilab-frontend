(function () {
    var clients = angular.module('ilab.clients');
    clients.service('Phone', ['Restangular', function (Restangular) {
        var phone =  Restangular
            .withConfig(function(RestangularConfigurer){
                RestangularConfigurer.setRestangularFields({
                    id:'Phone_ID'
                })
            }).service('Phone');

        phone.types = Restangular.service('Phone_Type');
        phone.saveAllPhones = function(phones){
            var path = _.reduce(phones, function(accum, n){
                return accum + n.Phone_ID + ',';
            },'');
            return phones.customPUT(phones, path);
        };
        return phone;
    }]);
})();
(function () {
    var company = angular.module('ilab.clients');
    company.controller('CompanyCtrl', ['$scope', '$routeParams', 'Company', 'Entity', 'Address', 'Designation', 'Referral', 'Individual', '$location', 'Employee', 'toaster',
        function ($scope, $routeParams, Company, Entity, Address, Designation, Referral, Individual, $location, Employee, toaster) {
            $scope.company = {};
            $scope.address = {};
            $scope.designation = {};
            $scope.individualOptions = [];
            $scope.referral = {};
            $scope.referral_types = [];

            $scope.states = [
                {
                    "name": "Alabama",
                    "abbreviation": "AL"
                },
                {
                    "name": "Alaska",
                    "abbreviation": "AK"
                },
                {
                    "name": "American Samoa",
                    "abbreviation": "AS"
                },
                {
                    "name": "Arizona",
                    "abbreviation": "AZ"
                },
                {
                    "name": "Arkansas",
                    "abbreviation": "AR"
                },
                {
                    "name": "California",
                    "abbreviation": "CA"
                },
                {
                    "name": "Colorado",
                    "abbreviation": "CO"
                },
                {
                    "name": "Connecticut",
                    "abbreviation": "CT"
                },
                {
                    "name": "Delaware",
                    "abbreviation": "DE"
                },
                {
                    "name": "District Of Columbia",
                    "abbreviation": "DC"
                },
                {
                    "name": "Federated States Of Micronesia",
                    "abbreviation": "FM"
                },
                {
                    "name": "Florida",
                    "abbreviation": "FL"
                },
                {
                    "name": "Georgia",
                    "abbreviation": "GA"
                },
                {
                    "name": "Guam",
                    "abbreviation": "GU"
                },
                {
                    "name": "Hawaii",
                    "abbreviation": "HI"
                },
                {
                    "name": "Idaho",
                    "abbreviation": "ID"
                },
                {
                    "name": "Illinois",
                    "abbreviation": "IL"
                },
                {
                    "name": "Indiana",
                    "abbreviation": "IN"
                },
                {
                    "name": "Iowa",
                    "abbreviation": "IA"
                },
                {
                    "name": "Kansas",
                    "abbreviation": "KS"
                },
                {
                    "name": "Kentucky",
                    "abbreviation": "KY"
                },
                {
                    "name": "Louisiana",
                    "abbreviation": "LA"
                },
                {
                    "name": "Maine",
                    "abbreviation": "ME"
                },
                {
                    "name": "Marshall Islands",
                    "abbreviation": "MH"
                },
                {
                    "name": "Maryland",
                    "abbreviation": "MD"
                },
                {
                    "name": "Massachusetts",
                    "abbreviation": "MA"
                },
                {
                    "name": "Michigan",
                    "abbreviation": "MI"
                },
                {
                    "name": "Minnesota",
                    "abbreviation": "MN"
                },
                {
                    "name": "Mississippi",
                    "abbreviation": "MS"
                },
                {
                    "name": "Missouri",
                    "abbreviation": "MO"
                },
                {
                    "name": "Montana",
                    "abbreviation": "MT"
                },
                {
                    "name": "Nebraska",
                    "abbreviation": "NE"
                },
                {
                    "name": "Nevada",
                    "abbreviation": "NV"
                },
                {
                    "name": "New Hampshire",
                    "abbreviation": "NH"
                },
                {
                    "name": "New Jersey",
                    "abbreviation": "NJ"
                },
                {
                    "name": "New Mexico",
                    "abbreviation": "NM"
                },
                {
                    "name": "New York",
                    "abbreviation": "NY"
                },
                {
                    "name": "North Carolina",
                    "abbreviation": "NC"
                },
                {
                    "name": "North Dakota",
                    "abbreviation": "ND"
                },
                {
                    "name": "Northern Mariana Islands",
                    "abbreviation": "MP"
                },
                {
                    "name": "Ohio",
                    "abbreviation": "OH"
                },
                {
                    "name": "Oklahoma",
                    "abbreviation": "OK"
                },
                {
                    "name": "Oregon",
                    "abbreviation": "OR"
                },
                {
                    "name": "Palau",
                    "abbreviation": "PW"
                },
                {
                    "name": "Pennsylvania",
                    "abbreviation": "PA"
                },
                {
                    "name": "Puerto Rico",
                    "abbreviation": "PR"
                },
                {
                    "name": "Rhode Island",
                    "abbreviation": "RI"
                },
                {
                    "name": "South Carolina",
                    "abbreviation": "SC"
                },
                {
                    "name": "South Dakota",
                    "abbreviation": "SD"
                },
                {
                    "name": "Tennessee",
                    "abbreviation": "TN"
                },
                {
                    "name": "Texas",
                    "abbreviation": "TX"
                },
                {
                    "name": "Utah",
                    "abbreviation": "UT"
                },
                {
                    "name": "Vermont",
                    "abbreviation": "VT"
                },
                {
                    "name": "Virgin Islands",
                    "abbreviation": "VI"
                },
                {
                    "name": "Virginia",
                    "abbreviation": "VA"
                },
                {
                    "name": "Washington",
                    "abbreviation": "WA"
                },
                {
                    "name": "West Virginia",
                    "abbreviation": "WV"
                },
                {
                    "name": "Wisconsin",
                    "abbreviation": "WI"
                },
                {
                    "name": "Wyoming",
                    "abbreviation": "WY"
                }
            ];

            var getEntityId = function () {
                    return Entity.post({Entity_Type: "Company"}).then(function (entityID) {
                        $scope.company.Entity_ID = entityID;
                        return {
                            entityId: entityID
                        };
                    });
                },
                getNewCompanyId = function (entity_Id) {
                    return Company.post({Entity_ID: entity_Id.entityId}).then(function (companyId) {
                        $scope.company.Company_ID = companyId
                        return {
                            entityId: entity_Id.entityId,
                            companyId: companyId
                        }
                    })
                        .then(loadCompanyDetails);
                },
                loadCompanyDetails = function (companyInfo) {
                    return Company.get(companyInfo.companyId)
                        .then(function (company) {
                            $scope.company = company;
                            $scope.company.Membership_Date = new Date(company.Membership_Date);
                            $scope.company.Date_Formed = new Date(company.Date_Formed);
                            return {
                                companyId: company.Company_ID,
                                entityId: company.Entity_ID
                            }
                        });
                },
                loadAddress = function (entityInfo) {
                    return Address
                        .getList({transform: 1, filter: 'Entity_ID,eq,' + entityInfo.entityId})
                        .then(function (addressInfo) {
                            $scope.addresses = addressInfo;
                            $scope.address = $scope.addresses[0];
                            return entityInfo;
                        });
                },
                loadDesignation = function (companyInfo) {
                    return Designation
                        .getList({transform: 1, filter: 'Company_ID,eq,' + companyInfo.companyId})
                        .then(function (designationInfo) {
                            $scope.designations = designationInfo;
                            $scope.designation = $scope.designations[0];
                            return companyInfo;
                        });
                },
                loadReferral = function (companyInfo) {
                    return Referral
                        .getList({
                            transform: 1,
                            filter: 'Referree_Entity_ID,eq,' + companyInfo.entityId,
                            include: 'Entity,Company'
                        })
                        .then(function (referralInfo) {
                            $scope.referrals = referralInfo;
                            $scope.referral = $scope.referrals[0];
                            return companyInfo;
                        });
                },
                loadDesignationTypes = function () {
                    Designation
                        .types
                        .getList({transform: 1})
                        .then(function (response) {
                            $scope.designationTypes = response;
                        });
                };


            var saveAddress = function () {
                    if (!$scope.addresses.length && $scope.address.Address_1) {
                        $scope.address.Entity_ID = $scope.company.Entity_ID;
                        return Address.post($scope.address);
                    } else if ($scope.addresses.length) {
                        return Address.saveAllAddresses($scope.addresses);
                    }

                },
                saveDesignation = function () {
                    if (!$scope.designations.length) {
                        $scope.designation.Company_ID = $scope.company.Company_ID;
                        return Designation.post($scope.designation);
                    } else if ($scope.designations.length) {
                        return Designation.saveAllDesignations($scope.designations)
                    }
                },
                saveReferral = function () {
                    if (!$scope.referrals.length) {
                        $scope.referral.Referree_Entity_ID = $scope.company.Entity_ID;
                        $scope.referral.Referral_Type = "Individual";
                        return Referral.post($scope.referral)
                    } else if ($scope.referrals.length) {
                        return Referral.saveAllReferrals($scope.referrals)
                    }
                }

            $scope.AddReferral = function () {
                var dummyReferral = {
                    Referral_Type: "website",
                    Referrer_Entity_ID: 1,
                    Referree_Entity_ID: $scope.company.Entity_ID,
                    Notes: "This is a note"
                };
                Referral.post(dummyReferral).then(function (data) {
                    if (data) {
                        loadReferral({
                            entityId: $scope.company.Entity_ID
                        });
                    }
                });
            };

            $scope.AddDesignation = function () {
                var dummyDesignation = {
                    Designation_Type: "Type",
                    Notes: "Designation description",
                    Company_ID: $scope.company.Company_ID
                };
                Designation.post(dummyDesignation).then(function (data) {
                    if (data) {
                        loadDesignation({
                            companyId: $scope.company.Company_ID
                        });
                    }
                });
            };

            $scope.deleteEntity = function () {
                $scope.company
                    .remove()
                    .then(function (response) {
                        $location.path('/companies');
                    });
            };

            $scope.removeDesignation = function (index) {
                $scope.designations[index]
                    .remove()
                    .then(function () {
                        loadDesignation({companyId: $scope.company.Company_ID});
                    });
            };

            var getListOfIndividuals = function () {
                return list = Individual.getList({transform: 1})
                    .then(function (listOfIndividuals) {
                        $scope.individualOptions = listOfIndividuals;
                    })
            };

            var getListOfReferralTypes = function () {
                    return Referral.types.getList({transform: 1})
                        .then(function (listOfReferrals) {
                            $scope.referral_types = listOfReferrals;
                        });
                };

            $scope.saveBasicCompanyInfo = function () {
                if ($scope.company.Company_ID) {
                    $scope.company
                        .put()
                        .then(function (response) {

                        });
                    saveAddress()
                        .then(function (response) {
                            if (response) {
                                toaster.pop('success', 'Saved Successfully',
                                    $scope.company.Name + "'s info saved")
                            } else {
                                toaster.pop('error', 'looks like something is not right try saving again')
                            }
                        });
                }
            };

            $scope.addAdditionalCompanyInfo = function (companyInfo) {
                $scope.company
                    .put()
                    .then(function (response) {
                        if (response) {
                            toaster.pop('success', 'Saved Successfully',
                                $scope.company.Name + "'s info saved")
                        } else {
                            toaster.pop('error', 'looks like something is not right try saving again')
                        }
                    });
            };
            $scope.addDesignationInfo = function () {
                return saveDesignation()
                    .then(function (response) {
                        if (response) {
                            toaster.pop('success', 'Desingation Info Saved Successfully!');
                        } else {
                            toaster.pop('error', 'looks like something is not right try saving again')
                        }
                    });
            };
            $scope.addReferralInfo = function () {
                return saveReferral()
                    .then(function (response) {
                        if (response) {
                            toaster.pop('success', 'Referral Info Saved Successfully',
                                $scope.company.Name + "'s info saved")
                        } else {
                            toaster.pop('error', 'looks like something is not right try saving again')
                        }
                    });
            };

            function init() {
                if ($location.path().match('add')) {
                    getEntityId()
                        .then(getNewCompanyId)
                        .then(loadAddress)
                        .then(loadDesignation)
                        .then(loadReferral);
                    getListOfIndividuals();

                } else {
                    loadCompanyDetails({companyId: $routeParams.id})
                        .then(loadAddress)
                        .then(loadDesignation)
                        .then(loadReferral);
                    getListOfIndividuals();

                }
                loadDesignationTypes();
                getListOfReferralTypes();
            }

            init();
        }]);
})();
(function () {
    var clients = angular.module('ilab.clients');
    clients.service('Referral', ['Restangular', function (Restangular) {
        var referral = Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setRestangularFields({
                id: 'Referral_ID'
            })
        }).service('Referral');

        referral.saveAllReferrals = function (referrals) {
            var path = _.reduce(referrals, function (accum, n) {
                return accum + n.Referral_ID + ',';
            }, '');
            return referrals.customPUT(referrals, path);
        };
        referral.types = Restangular.service('Referral_Type');
        return referral;


    }]);
})();
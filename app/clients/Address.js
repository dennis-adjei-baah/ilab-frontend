(function () {
    var clients = angular.module('ilab.clients');
    clients.service('Address', ['Restangular', function (Restangular) {
        var address = Restangular.withConfig(function(RestangularConfigurer){
            RestangularConfigurer.setRestangularFields({
                id:'Address_ID'
            })
        }).service('Address');

        address.saveAllAddresses = function(addresses){
            var path = _.reduce(addresses, function(accum, n){
                return accum + n.Address_ID + ',';
            },'');
            return addresses.customPUT(addresses, path);
        };
        return address;
    }]);
})();
(function () {
    var client = angular.module('ilab.clients');
    client.controller('IndividualCtrl',
        ['$scope', '$routeParams', '$location', 'Individual', 'Address', 'Phone', 'Employee', 'Demographic', 'Company', 'Entity', 'toaster',
            function ($scope, $routeParams, $location, Individual, Address, Phone, Employee, Demographic, Company, Entity, toaster) {
                $scope.individual = {};


                var loadProfileInfo = function (individualId) {
                        return Individual
                            .get(individualId)
                            .then(function (individual) {
                                $scope.individual = individual;
                                return {
                                    individualId: individual.Individual_ID,
                                    entityId: individual.Entity_ID
                                }
                            });
                    },
                    loadDemographic = function (entityInfo) {
                        return Demographic.getList({
                            transform: 1,
                            filter: 'Individual_ID,eq,' + entityInfo.individualId
                        })
                            .then(function (demographic) {
                                $scope.demographics = demographic;
                                return entityInfo;
                            });
                    },
                    loadAddress = function (entityInfo) {
                        return Address
                            .getList({transform: 1, filter: 'Entity_ID,eq,' + entityInfo.entityId})
                            .then(function (addressInfo) {
                                $scope.addresses = addressInfo
                                return entityInfo;
                            });
                    },
                    loadPhone = function (entityInfo) {
                        return Phone
                            .getList({transform: 1, filter: 'Individual_ID,eq,' + entityInfo.individualId})
                            .then(function (phoneInfo) {
                                $scope.phones = phoneInfo;
                                return entityInfo;
                            })
                    },
                    loadPhoneTypes = function () {
                        Phone.types.getList({transform: 1}).then(function (types) {
                            $scope.phoneTypes = types;
                        });
                    },
                    loadCompanyInfo = function (entityInfo) {
                        return Employee
                            .getList({
                                transform: 1,
                                filter: 'Individual_ID,eq,' + entityInfo.individualId,
                                include: 'Company'
                            })
                            .then(function (employeeInfo) {
                                $scope.employeeInfo = _.flatMap(employeeInfo, function (employee) {
                                    return _.map(employee.Company, function (company) {
                                        return _.defaults({
                                            "Company_Name": company.Name,
                                            "Company_ID": company.Company_ID
                                        }, {Role: employee.Role});
                                    });
                                });
                            })
                    },
                    loadCompanyValues = function () {
                        Company
                            .getList({transform: 1, columns: 'Name,Company_ID'})
                            .then(function (companyList) {
                                $scope.companyValues = companyList;
                            })
                    },
                    loadDemographicTypes = function () {
                        Demographic.types.getList({transform: 1}).then(function (response) {
                            $scope.demographicTypes = response;
                        });
                    },
                    getFullName = function (individual) {
                        return individual.Honorific + ' '
                            + individual.First_Name + ' '
                            + individual.Last_Name;
                    },
                    getNewEntityId = function () {
                        return Entity.post({Entity_Type: "Individual"}).then(function (entityId) {
                            $scope.individual.Entity_ID = entityId;
                            return {
                                entityId: entityId
                            };
                        });
                    },
                    getNewIndividualId = function (entityInfo) {
                        Individual.post({Entity_ID: entityInfo.entityId}).then(function (individualId) {
                            return {
                                entityId: entityInfo.entityId,
                                individualId: individualId
                            }
                        }).then(function (entityInfo) {
                            loadProfileInfo(entityInfo.individualId);
                        });
                    };

                $scope.states = [
                    {
                        "name": "Alabama",
                        "abbreviation": "AL"
                    },
                    {
                        "name": "Alaska",
                        "abbreviation": "AK"
                    },
                    {
                        "name": "American Samoa",
                        "abbreviation": "AS"
                    },
                    {
                        "name": "Arizona",
                        "abbreviation": "AZ"
                    },
                    {
                        "name": "Arkansas",
                        "abbreviation": "AR"
                    },
                    {
                        "name": "California",
                        "abbreviation": "CA"
                    },
                    {
                        "name": "Colorado",
                        "abbreviation": "CO"
                    },
                    {
                        "name": "Connecticut",
                        "abbreviation": "CT"
                    },
                    {
                        "name": "Delaware",
                        "abbreviation": "DE"
                    },
                    {
                        "name": "District Of Columbia",
                        "abbreviation": "DC"
                    },
                    {
                        "name": "Federated States Of Micronesia",
                        "abbreviation": "FM"
                    },
                    {
                        "name": "Florida",
                        "abbreviation": "FL"
                    },
                    {
                        "name": "Georgia",
                        "abbreviation": "GA"
                    },
                    {
                        "name": "Guam",
                        "abbreviation": "GU"
                    },
                    {
                        "name": "Hawaii",
                        "abbreviation": "HI"
                    },
                    {
                        "name": "Idaho",
                        "abbreviation": "ID"
                    },
                    {
                        "name": "Illinois",
                        "abbreviation": "IL"
                    },
                    {
                        "name": "Indiana",
                        "abbreviation": "IN"
                    },
                    {
                        "name": "Iowa",
                        "abbreviation": "IA"
                    },
                    {
                        "name": "Kansas",
                        "abbreviation": "KS"
                    },
                    {
                        "name": "Kentucky",
                        "abbreviation": "KY"
                    },
                    {
                        "name": "Louisiana",
                        "abbreviation": "LA"
                    },
                    {
                        "name": "Maine",
                        "abbreviation": "ME"
                    },
                    {
                        "name": "Marshall Islands",
                        "abbreviation": "MH"
                    },
                    {
                        "name": "Maryland",
                        "abbreviation": "MD"
                    },
                    {
                        "name": "Massachusetts",
                        "abbreviation": "MA"
                    },
                    {
                        "name": "Michigan",
                        "abbreviation": "MI"
                    },
                    {
                        "name": "Minnesota",
                        "abbreviation": "MN"
                    },
                    {
                        "name": "Mississippi",
                        "abbreviation": "MS"
                    },
                    {
                        "name": "Missouri",
                        "abbreviation": "MO"
                    },
                    {
                        "name": "Montana",
                        "abbreviation": "MT"
                    },
                    {
                        "name": "Nebraska",
                        "abbreviation": "NE"
                    },
                    {
                        "name": "Nevada",
                        "abbreviation": "NV"
                    },
                    {
                        "name": "New Hampshire",
                        "abbreviation": "NH"
                    },
                    {
                        "name": "New Jersey",
                        "abbreviation": "NJ"
                    },
                    {
                        "name": "New Mexico",
                        "abbreviation": "NM"
                    },
                    {
                        "name": "New York",
                        "abbreviation": "NY"
                    },
                    {
                        "name": "North Carolina",
                        "abbreviation": "NC"
                    },
                    {
                        "name": "North Dakota",
                        "abbreviation": "ND"
                    },
                    {
                        "name": "Northern Mariana Islands",
                        "abbreviation": "MP"
                    },
                    {
                        "name": "Ohio",
                        "abbreviation": "OH"
                    },
                    {
                        "name": "Oklahoma",
                        "abbreviation": "OK"
                    },
                    {
                        "name": "Oregon",
                        "abbreviation": "OR"
                    },
                    {
                        "name": "Palau",
                        "abbreviation": "PW"
                    },
                    {
                        "name": "Pennsylvania",
                        "abbreviation": "PA"
                    },
                    {
                        "name": "Puerto Rico",
                        "abbreviation": "PR"
                    },
                    {
                        "name": "Rhode Island",
                        "abbreviation": "RI"
                    },
                    {
                        "name": "South Carolina",
                        "abbreviation": "SC"
                    },
                    {
                        "name": "South Dakota",
                        "abbreviation": "SD"
                    },
                    {
                        "name": "Tennessee",
                        "abbreviation": "TN"
                    },
                    {
                        "name": "Texas",
                        "abbreviation": "TX"
                    },
                    {
                        "name": "Utah",
                        "abbreviation": "UT"
                    },
                    {
                        "name": "Vermont",
                        "abbreviation": "VT"
                    },
                    {
                        "name": "Virgin Islands",
                        "abbreviation": "VI"
                    },
                    {
                        "name": "Virginia",
                        "abbreviation": "VA"
                    },
                    {
                        "name": "Washington",
                        "abbreviation": "WA"
                    },
                    {
                        "name": "West Virginia",
                        "abbreviation": "WV"
                    },
                    {
                        "name": "Wisconsin",
                        "abbreviation": "WI"
                    },
                    {
                        "name": "Wyoming",
                        "abbreviation": "WY"
                    }
                ];

                $scope.deleteEntity = function () {
                    $scope.individual
                        .remove()
                        .then(function(response){
                        $location.path('/individuals');
                    });
                };

                $scope.deletePhone = function(index){
                    $scope.phones[index].remove()
                        .then(function(){
                            loadPhone({individualId:$scope.individual.Individual_ID})
                        });
                };

                $scope.addPhone = function () {
                    var dummyPhone = {
                        Number: "123-456-7890",
                        IsPrimary: 0,
                        Individual_ID: $scope.individual.Individual_ID,
                        Phone_Type: "Home"
                    };
                    Phone.post(dummyPhone).then(function (data) {
                        if (data) {
                            loadPhone({individualId: $scope.individual.Individual_ID})
                        }
                    })
                };

                $scope.addCompany = function () {
                    $scope.employment.Individual_ID = $scope.individual.Individual_ID;
                    Employee.post($scope.employment).then(function (response) {
                        if (response) {
                            loadCompanyInfo({individualId:$scope.individual.Individual_ID});
                        }
                    });
                };

                $scope.addDemographic = function (demographicItem) {
                    Demographic.post({
                        Demographic_Type: demographicItem.Demographic_Type,
                        Individual_ID: $scope.individual.Individual_ID
                    }).then(function(response){
                        if(response){
                            loadDemographic({individualId:$scope.individual.Individual_ID});
                        }
                    });
                };

                $scope.deleteDemographic = function(demographicItem){
                    demographicItem
                        .remove()
                        .then(function(response){
                           if(response){
                               loadDemographic({individualId:$scope.individual.Individual_ID});
                           }
                        });
                };

                $scope.addAddress = function () {
                    var dummyAddress = {
                        Address_1: "123 Main St.",
                        Address_2: "",
                        Address_Type: "Work",
                        City: "Johnson City",
                        Country: 1,
                        Entity_ID: $scope.individual.Entity_ID,
                        Postal_Code: "00000",
                        State: "TN"
                    };
                    Address.post(dummyAddress).then(function (data) {
                        if (data) {
                            loadAddress({
                                entityId: $scope.individual.Entity_ID
                            });
                        }
                    });
                };

                $scope.saveBasicInfo = function () {
                    if ($scope.individual.Individual_ID) {
                        $scope.individual
                            .put()
                            .then(function (response) {
                                if (response) {
                                    toaster.pop('success', 'Saved Successfully!',
                                        getFullName($scope.individual) + "'s info saved");
                                } else {
                                    toaster.pop('error', 'Oops', 'looks like something is not right, try saving again.')
                                }

                            });
                        Address.saveAllAddresses($scope.addresses).then(function (response) {
                        });
                        Phone.saveAllPhones($scope.phones).then(function (response) {

                        });
                    }
                };

                function init() {
                    if ($location.path().match('add')) {
                        getNewEntityId()
                            .then(getNewIndividualId);
                    } else {
                        loadProfileInfo($routeParams.id)
                            .then(loadAddress)
                            .then(loadDemographic)
                            .then(loadPhone)
                            .then(loadCompanyInfo);
                        loadPhoneTypes();
                        loadDemographicTypes();
                        loadCompanyValues();
                    }
                }

                init();
            }]);
})();
(function () {
    var clients = angular.module('ilab.clients');
    clients.service('Company', ['Restangular', function (Restangular) {
        return Restangular.withConfig(function(RestangularConfigurer){
           RestangularConfigurer.setRestangularFields({
               id:'Company_ID'
           })
        }).service('Company');
    }]);
})();
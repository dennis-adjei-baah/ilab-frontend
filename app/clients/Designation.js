(function () {
    var clients = angular.module('ilab.clients');
    clients.service('Designation', ['Restangular', function (Restangular) {
        var designation = Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setRestangularFields({
                id: 'Designation_ID'
            })
        }).service('Designation');

        designation.saveAllDesignations = function (designations) {
            var path = _.reduce(designations, function (accum, n) {
                return accum + n.Designation_ID + ',';
            }, '');
            return designations.customPUT(designations, path);
        };
        designation.types = Restangular.service('Designation_Type');
        return designation;


    }]);
})();
(function () {
    var clients = angular.module('ilab.clients');
    clients.service('Demographic', ['Restangular', function (Restangular) {
        var demographic = Restangular.withConfig(function(RestangularConfigurer){
            RestangularConfigurer.setRestangularFields({
                id:'Demographic_ID'
            })
        }).service('Demographic');
        demographic.types = Restangular.service('Demographic_Type');
        return demographic;
    }]);
})();
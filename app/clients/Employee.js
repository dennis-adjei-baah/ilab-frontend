(function () {
    var clients = angular.module('ilab.clients');
    clients.service('Employee', ['Restangular', function (Restangular) {
        return Restangular.service('Employee');
    }]);
})();
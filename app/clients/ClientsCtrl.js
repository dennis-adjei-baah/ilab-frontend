(function () {
    var clients = angular.module('ilab.clients');
    clients.controller('ClientsCtrl', ['$scope', 'Company', 'Individual', '$location', function ($scope, Company, Individual, $location) {
        $scope.clients = [];
        $scope.currentLocation = $location.path();
        $scope.navTitle = 'Companies';
        $scope.routeClient = function (client) {
            if(client.Company_ID){
                $location.path('/companies/' + client.Company_ID);
            }else if(client.Individual_ID){
                $location.path('/individuals/' + client.Individual_ID)
            }
        };

        function loadClientInfo() {
            if ($location.path().match('companies')) {
                Company.getList({transform: 1}).then(function (companies) {
                    $scope.clients = companies;
                    $scope.navTitle = 'Companies';
                });
            } else if ($location.path().match('individuals')) {
                Individual.getList({transform: 1, filter:'Is_Active,eq,'+1}).then(function (individuals) {
                    $scope.clients = individuals;
                    $scope.navTitle = 'Individuals';
                });
            }
        }

        loadClientInfo();

    }]);
})(window.angular);
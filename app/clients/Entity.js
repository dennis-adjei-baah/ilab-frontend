(function(){
    var clients = angular.module('ilab.clients');
    clients.service('Entity', ['Restangular', function(Restangular){
      var entity = Restangular.withConfig(function(RestangularConfigurer){
          RestangularConfigurer.setRestangularFields({
              id:'Entity_ID'
          })
      }).service('Entity');
      return entity;
    }])
})();
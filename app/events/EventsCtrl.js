
(function () {
    var events = angular.module('ilab.events');
    events.controller('EventsCtrl', ['$scope', 'EventList', '$location', function ($scope, EventList, $location) {

        $scope.events = [];
        $scope.currentLocation = $location.path();

        function loadEventInfo(){
            if ($location.path().match('events')) {
                EventList.getList({transform: 1}).then(function(events) {
                    $scope.events = events;
                });
            }
        }


        loadEventInfo();

    }]);
})(window.angular);
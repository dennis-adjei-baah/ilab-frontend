(function () {
    var events = angular.module('ilab.events');
    events.service('EventList', ['Restangular', function (Restangular) {
        var eventlist = Restangular.service('EventList');
        return eventlist;
    }])
})();
(function () {
    var reports = angular.module('ilab.reports');
    reports.controller("ReportsCtrl", ['$scope', 'StoredProcService', function ($scope, StoredProcService) {
        $scope.chartData = [];
        var currentYear = new Date().getFullYear();
        var preChartData = {};
        $scope.options = {
            chart: {
                type: 'lineChart',
                height: 450,
                margin: {
                    top: 20,
                    right: 20,
                    bottom: 60,
                    left: 55
                },
                x: function (d) {
                    return d.x;
                },
                y: function (d) {
                    return d.y;
                },
                showValues: true,
                transitionDuration: 500,
                xAxis: {
                    axisLabel: 'YoY (Year over Year)'
                },
                yAxis: {
                    axisLabel: '',
                    axisLabelDistance: 0,
                    tickFormat: function (d) {
                        if(d >= 500){
                            return d3.format('$,')(d);
                        }
                        else{
                            return d3.format(',')(d);
                        }
                    }
                }
            }
        };
        var compileAllReports = function (reportData) {
                console.log(preChartData);
                var chart = {};
                if (preChartData.clients) {
                    angular.forEach(preChartData.clients, function (value, key) {
                        chart[key] = (chart[key] || []).concat([{
                            key: 'Clients',
                            values: value,
                            area: false,
                            type: "bar",
                            color: '#8cbc4f'
                        }])
                    });
                }
                if (preChartData.graduates) {
                    angular.forEach(preChartData.graduates, function (value, key) {
                        chart[key] = (chart[key] || []).concat([{
                            key: 'Graduates',
                            values: value,
                            area: false,
                            type: "bar",
                            color: "#1d73aa"
                        }])
                    });
                }

                $scope.chartData = chart;
            },
            loadClientData = function () {
                return StoredProcService
                    .getAllReports($scope.clientSelector.startYear, $scope.clientSelector.endYear, 'Client')
                    .then(function (chartData) {
                        preChartData.clients = chartData;
                        return true;
                    });
            },
            loadGraduateData = function (chartData) {
                return StoredProcService
                    .getAllReports($scope.clientSelector.startYear, $scope.clientSelector.endYear, 'Graduate')
                    .then(function (chartData) {
                        preChartData.graduates = chartData;
                        return true;
                    });
            },
            clearChartData = function () {
                preChartData = {};
                angular.forEach($scope.chartData, function (value, key) {
                    $scope.chartData[key] = [];
                });
            };

        $scope.clientSelector = {
            isClients: 1,
            isGraduates: 1,
            startYear: currentYear - 5,
            endYear: currentYear
        };

        $scope.filterReports = function () {

        };

        $scope.reloadChartData = function () {

            if ($scope.clientSelector.isClients && $scope.clientSelector.isGraduates) {
                loadClientData()
                    .then(loadGraduateData)
                    .then(compileAllReports)
            } else if ($scope.clientSelector.isClients) {
                preChartData.graduates = null;
                loadClientData()
                    .then(compileAllReports);
            }
            else if ($scope.clientSelector.isGraduates) {
                preChartData.clients = null;
                loadGraduateData()
                    .then(compileAllReports);
            }
            clearChartData();
        };


        function init() {
            loadClientData()
                .then(loadGraduateData)
                .then(compileAllReports)
        }

        init();
    }]);

})(window.angular);

(function () {
    var clients = angular.module('ilab.reports');
    clients.factory('StoredProcService', ['$http', function ($http) {
        var promise;
        var service = {
            getAllReports: function (minYear, maxYear, clientStatus) {
                promise = $http.get('http://www.cusaac.com/apiext.php/Sum_By_Year_Agg?transform=1&procedure=true&minDate='+minYear+'&maxDate='+maxYear+'&clientStatus='+clientStatus)
                    .then(function (response) {
                        return service.groupByReportType(response.data.table);
                    });
                return promise;
            },
            getAllGraduateReports: function (minYear, maxYear) {

            },
            reportTypeQuery:function(reportType){
                if(reportType === 'Client'){
                    return 'http://www.cusaac.com/apiext.php/Sum_By_Year_Agg?transform=1&procedure=true&minDate=2010&maxDate=2020&clientStatus=Client';
                }
                else{
                    return 'http://www.cusaac.com/apiext.php/Sum_By_Year_Agg?transform=1&procedure=true&minDate=2010&maxDate=2020&clientStatus=Graduate'
                }
            },
            groupByReportType: function (reports) {
                var result = {};
                angular.forEach(reports, function (value, key) {
                    var year = value.Year;
                    angular.forEach(value, function (value, key) {
                        if (key != 'Year') {
                            result[key] = (result[key] || []).concat([{'x': year.toString(), 'y': Number(value)}]);
                        }
                    });
                });
                return result;
            }
        };
        return service;
    }]);
})();
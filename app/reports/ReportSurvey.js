(function () {
    var clients = angular.module('ilab.clients');
    clients.service('ReportSurvey', ['Restangular', function (Restangular) {
        var ReportSurvey =  Restangular.service('ReportData');
        ReportSurvey.aggregateReports = Restangular.service('Sum_By_Year_Agg');
        return ReportSurvey;
    }]);
})();
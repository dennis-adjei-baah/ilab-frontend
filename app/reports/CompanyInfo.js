(function () {
    var clients = angular.module('ilab.clients');
    clients.service('CompanyInfo', ['Restangular', function (Restangular) {
        return Restangular.service('CompanyInfo');
    }]);
})();
# iLab SPA #

This is the frontend to the iLab client tracking system

### What is this repository for? ###



### How do I get set up? ###

- Install node if you do not have it at [NodeJS website](https://nodejs.org/en/)

- pull down this repository in a directory of your choice with
``` 
#!bash
git clone https://dennis-adjei-baah@bitbucket.org/dennis-adjei-baah/ilab-frontend.git
```
- Open up a node command prompt (in Windows) or a terminal (in UNIX)

- cd into the directory you cloned the project into and run 

```
#!bash
npm install
```

- Install the grunt cli to use grunt and its other tasks 
```
#!bash
npm install -g grunt-cli
```

- After npm install an the grunt-cli is finished loading, build the project
```
#!bash
grunt build
```
- Then start the whole node process with npm start, that should start a local server and pull up the site in your browser.
```
#!bash
npm start
```